function result = chooseFunction(id,handles)
fstrTable=strings(17,1);

fstpotential='@(x,y)x^2+y^2'; % 2.1 Dejong
fstrTable(1) = fstpotential; % 2.1 Dejong
fstpotential='@(x,y)x^2+(x+y)^2'; % 2.2 Axis parallel hyper-ellipsoid function
fstrTable(2) = fstpotential;
fstpotential='@(x,y)2*x^2+y^2'; % 2.3 Rotated hyper-ellipsoid function
fstrTable(3) = fstpotential;
fstpotential='@(x,y)5*x^2+10*y^2'; % 2.4 Moved axis parallel hyper-ellipsoid function
fstrTable(4) = fstpotential;
fstpotential='@(x,y)100*(y-x^2)^2+(1-x)^2'; % 2.5 Rosenbrock's valley (De Jong's function 2) inny wynik
fstrTable(5) = fstpotential;
fstpotential='@(x,y)20+x^2-10*cos(2*pi*x)+y^2-10*cos(2*pi*y)'; % 2.6 Rastrigin's function 6
fstrTable(6) = fstpotential;
fstpotential='@(x,y)-x*sin(sqrt(abs(x)))-y*sin(sqrt(abs(y)))'; % 2.7 Schwefel' function 7 
fstrTable(7) = fstpotential;
fstpotential='@(x,y)x^2/4000+y^2/4000-cos(x)*cos(y/sqrt(2))+1'; %2.8 Griewangk's function 8 inny wynik, bo to s� lokalne
fstrTable(8) = fstpotential;
fstpotential='@(x,y)x^2+abs(y)^3'; % 2.9 Sum of different power function 9 potrzeba wi�cej iteracji
fstrTable(9) = fstpotential;
fstpotential='@(x,y,a,b,c)-a*exp(-b*sqrt((x+y)/2))-exp((cos(c*x)+cos(c*y))/2)+20+exp(1)'; % 2.10 Ackley's Path function 10
fstrTable(10) = fstpotential;
%%koniec
result=fstrTable(id);
result=char(result);
transpose(result);
handles.txtFunction.String=strcat('Rozwa�ana funkcja:',result);