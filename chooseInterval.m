function interval = chooseInteval(id)
itbl=[-5.12 5.12];%Tablica przedziałów %2.1
itbl=[itbl;-5.12 5.12]; %2.2
itbl=[itbl;-65.536 65.536]; %2.3
itbl=[itbl;-5.12 5.12]; %2.4
itbl=[itbl;-2.048 2.048]; %2.5
itbl=[itbl;-5.12 5.12]; %2.6
itbl=[itbl;-500 500]; %2.7
itbl=[itbl;-600 600];%2.8
itbl=[itbl;-1 1]; %2.9
itbl=[itbl;-1 1]; %2.10
itbl=[itbl;0 10]; %2.11
itbl=[itbl;0 pi]; %2.12
itbl=[itbl;-5 10]; %2.13 Tutaj jest tylko dla x1, dla x2 jest 0 15
itbl=[itbl;-100 100]; %2.14
itbl=[itbl;-2 2]; %2.15
itbl=[itbl;-3 3]; %2.16 Tutaj jest tylko dla x1, dla x2 jest -2 2
interval=itbl(id,:)
if id==13
    interval=[interval;0 15];
elseif id==16
    interval=[interval;-2 2];
else
    interval=[interval;interval];
end
