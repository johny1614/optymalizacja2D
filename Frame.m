function varargout = Frame(varargin)
% FRAME MATLAB code for Frame.fig
%      FRAME, by itself, creates a new FRAME or raises the existing
%      singleton*.
%
%      H = FRAME returns the handle to a new FRAME or the handle to
%      the existing singleton*.
%
%      FRAME('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FRAME.M with the given input arguments.
%
%      FRAME('Property','Value',...) creates a new FRAME or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Frame_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Frame_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Frame

% Last Modified by GUIDE v2.5 19-Apr-2017 13:12:36

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Frame_OpeningFcn, ...
                   'gui_OutputFcn',  @Frame_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Frame is made visible.
function Frame_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Frame (see VARARGIN)

% Choose default command line output for Frame
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Frame wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Frame_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function interval = getInterval(handles)
       minX=str2double(handles.editMinX.String);
       maxX=str2double(handles.editMaxX.String);
       minY=str2double(handles.editMinY.String);
       maxY=str2double(handles.editMaxY.String);
       interval=[minX maxX;minY maxY];

% --- Executes on button press in btnCalc.
function btnCalc_Callback(hObject, eventdata, handles)
    startPoint = [str2double(handles.editX.String),str2double(handles.editY.String)]
    interval=getInterval(handles);
    if handles.rbtnUser.Value
       fstr=strcat('@(x,y)',handles.editFn.String);
    else
        fstr=chooseFunction(handles.pop.Value,handles);
    end
    if handles.rbtnIteraton.Value %%Liczymy wedle ilosci iteracji
        iterations=handles.sliderIteration.Value;
        if interval(1,1)>=interval(1,2) || interval(2,1)>=interval(2,2)
            msgbox('Zdegenerowany przedzia�');
            return;
        end
        CalculateByIterations(fstr,startPoint,interval,iterations,handles);
    else %%Liczymy wedle minimalnego kroku
        minStep=str2double(handles.editStep.String);
        CalculateByStep(fstr,startPoint,interval,minStep,handles);
    end
% hObject    handle to btnCalc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function editFn_Callback(hObject, eventdata, handles)
% hObject    handle to editFn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editFn as text
%        str2double(get(hObject,'String')) returns contents of editFn as a double


% --- Executes during object creation, after setting all properties.
function editFn_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editFn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popFn.
function popFn_Callback(hObject, eventdata, handles)
interval=chooseInterval(hObject.Value);
% hObject    handle to popFn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popFn contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popFn


% --- Executes during object creation, after setting all properties.
function popFn_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popFn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in pop.
function pop_Callback(hObject, eventdata, handles)
interval=chooseInterval(hObject.Value);
handles.editMinX.String=interval(1,1);
handles.editMaxX.String=interval(1,2);
handles.editMinY.String=interval(2,1);
handles.editMaxY.String=interval(2,2);
functionstr=chooseFunction(handles.pop.Value);
functionstr=strrep(functionstr,'@(x,y)','f(x,y)=');
handles.txtFunction.String=functionstr;
% hObject    handle to pop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pop contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pop


% --- Executes during object creation, after setting all properties.
function pop_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function sliderIteration_Callback(hObject, eventdata, handles)
set(hObject,'Value',round(hObject.Value));
iterationCount=strcat('Ilo�� iteracji:',num2str(hObject.Value));
set(handles.txtIterationCount,'String',iterationCount);
%handles.txtIterationCount.String=strcat('Ilo�� iteracji:',num2str(hObject.Value));
% hObject    handle to sliderIteration (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function sliderIteration_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderIteration (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function editIterationCount_Callback(hObject, eventdata, handles)
% hObject    handle to editIterationCount (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editIterationCount as text
%        str2double(get(hObject,'String')) returns contents of editIterationCount as a double


% --- Executes during object creation, after setting all properties.
function editIterationCount_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editIterationCount (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editStep_Callback(hObject, eventdata, handles)
% hObject    handle to editStep (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editStep as text
%        str2double(get(hObject,'String')) returns contents of editStep as a double


% --- Executes during object creation, after setting all properties.
function editStep_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editStep (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editX_Callback(hObject, eventdata, handles)
% hObject    handle to editX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editX as text
%        str2double(get(hObject,'String')) returns contents of editX as a double


% --- Executes during object creation, after setting all properties.
function editX_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editY_Callback(hObject, eventdata, handles)
% hObject    handle to editY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editY as text
%        str2double(get(hObject,'String')) returns contents of editY as a double

function displayResults(xi,matx,f,handles)
try
    xi=eval(xi);
catch
end
    diffx=double(abs(xi(1)-matx(1))+abs(xi(2)-matx(2)));
    xi=double(xi);
    matx=double(matx);
    handles.txtFoundX.String=strcat('(x,y)=(',num2str(xi(1)),',',num2str(xi(2)),')');
    handles.txtFoundFX.String=strcat('f(x,y)=',num2str(f(xi(1),xi(2))));
    handles.txtToolX.String=strcat('(x,y)=(',num2str(matx(1)),',',num2str(matx(2)),')');
    handles.txtToolFX.String=strcat('f(x,y)=',num2str(f(matx(1),matx(2))));

% --- Executes during object creation, after setting all properties.
function editY_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function [xi,matx,diffx]=CalculateByIterations(fstr,startPoint,interval,iterations,handles)
f=str2func(fstr);%do postaci f(x,y)=x+y potrzebne by narysowa� wykres
fToolbox = @(x) f(x(1),x(2)); %do postaci f(x)=x(1)+x(2) potrzebne do fminsearch optimization toolboxa
matx=fminsearch(fToolbox,startPoint);
%Liczymy symbolicznie Fsym - potrzebne do policzenia gradientu
X = sym('x', [1 2]);
F = fToolbox(X);
Fsym=symfun(F,symvar(F));

gx1=-diff(Fsym,X(1));%pierwsza pochodna na minusie
gx2=-diff(Fsym,X(2));%druga pochodna na minusie

netSize=str2double(handles.editNet.String); %rozmiar siatki do rysowania
[XD,Y] = meshgrid(interval(1,1):netSize:interval(1,2),interval(2,1):netSize:interval(2,2)); %XD zeby nie dublowa� nazwy zmiennych. Przygotowanie dziedziny
Z=arrayfun(f,XD,Y);
window=figure;
sur=surface(XD,Y,Z,'ButtonDownFcn',{@clickThePoint});
hold on
scatter3(startPoint(1),startPoint(2),f(startPoint(1),startPoint(2)),70,'MarkerFaceColor',[0 0 1]);%rysowanie punkt�w na wykresie
if iterations == 0
    displayResults(startPoint,matx,f,handles);
    return;
end
s=[gx1(startPoint(1),startPoint(2)),gx2(startPoint(1),startPoint(2))];


%Szukanie lambdy 
fun = @(l) (f(startPoint(1)+s(1)*l,startPoint(2)+s(2)*l));
lambda = fminsearch(fun,0);

xi=startPoint+s*lambda; %nasz x1
scatter3(xi(1),xi(2),f(xi(1),xi(2)),70,'MarkerFaceColor',[1 0 0]);%rysowanie punkt�w na wykresie
xTable=[xi];
%%%%
%%Wedle algorytmu powinni�my teraz zinkrementowa� i, no ale nie mielibysmy
%%wtedy x2 do wyliczania s2. Wi�c liczymy tutaj s2 w oparciu o x0 i x1- kt�ry zmodyfikuje x2
licznik=gx1(xi(1),xi(2))^2+gx2(xi(1),xi(2))^2;
mianownik=gx1(startPoint(1),startPoint(2))^2+gx2(startPoint(1),startPoint(2))^2;
si=[gx1(xi(1),xi(2)),gx2(xi(1),xi(2))]+licznik/mianownik*s(1,:);
s=[s;si];
i=2;
%%I teraz liczymy x2 - zaczynaj�c od lambdy
%fun=@(l) (f(xTable(1,1)+l*si(1),xTable(1,2)+l*si(2)));
    %options = optimset('MaxFunEvals',100);
    %lambda = fminsearch(fun,0,options);
xi=xi+si*lambda; %nasz x2
scatter3(xi(1),xi(2),f(xi(1),xi(2)),'MarkerFaceColor',[1 0 0]);%rysowanie punkt�w na wykresie
tblLambda=lambda;
tblGradient=[100,100]
while i<iterations
    licznik=gx1(xi(1),xi(2))^2+gx2(xi(1),xi(2))^2;%norm(gx1(xi(1),xi(2)),gx2(xi(1),xi(2)))^2;
    mianownik=gx1(xTable(i-1,1),xTable(i-1,2))^2+gx2(xTable(i-1,1),xTable(i-1,2))^2;%norm(gx1(xTable(i-1,1),xTable(i-1,2)),gx2(xTable(i-1,1),xTable(i-1,2)))^2;
    si=[gx1(xi(1),xi(2)),gx2(xi(1),xi(2))]+licznik/mianownik*s(i,:);
    tblGradient=[tblGradient;[gx1(xi(1),xi(2)),gx2(xi(1),xi(2))]];
    s=[s;si];
    %Szukamy d�ugo�ci lambda
    %fun = @(l) (f(xTable(i-1,1)+l*si(1),xTable(i-1,2)+l*si(2)));   
    %options = optimset('MaxFunEvals',100000);
    %lambda = fminsearch(fun,0,options);
    
    tblLambda=[tblLambda;lambda];
    xi=xTable(i-1,:)+lambda*si;%liczymy aktualne xi po zinkrementowaniu
    scatter3(xi(1),xi(2),f(xi(1),xi(2)),70,'MarkerFaceColor',[1 0 0]);%rysowanie punkt�w na wykresie
    xi=round(xi*1000000)/1000000;
    if isnan(xi)%%kiedy juz zbiegli�my
        xi=xTable(i-1,:)
        break;
    end
    xTable=[xTable;xi];
    i=i+1
    xi
end

matx=fminsearch(fToolbox,startPoint);
displayResults(xi,matx,f,handles);


function [xi,matx,diffx]=CalculateByStep(fstr,startPoint,interval,minStep,handles)
f=str2func(fstr);%do postaci f(x,y)=x+y potrzebne by narysowa� wykres
fToolbox = @(x) f(x(1),x(2)); %do postaci f(x)=x(1)+x(2) potrzebne do fminsearch optimization toolboxa
matx=fminsearch(fToolbox,startPoint); %Wedle toolboxa
%Liczymy symbolicznie Fsym - potrzebne do policzenia gradientu
X = sym('x', [1 2]);
F = fToolbox(X);
Fsym=symfun(F,symvar(F));

gx1=-diff(Fsym,X(1));%pierwsza pochodna na minusie
gx2=-diff(Fsym,X(2));%druga pochodna na minusie

netSize=str2double(handles.editNet.String); %rozmiar siatki do rysowania
[XD,Y] = meshgrid(interval(1,1):netSize:interval(1,2),interval(2,1):netSize:interval(2,2)); %XD zeby nie dublowa� nazwy zmiennych. Przygotowanie dziedziny
Z=arrayfun(f,XD,Y);
window=figure;
sur=surface(XD,Y,Z,'ButtonDownFcn',{@clickThePoint});
hold on
scatter3(startPoint(1),startPoint(2),f(startPoint(1),startPoint(2)),70,'MarkerFaceColor',[0 0 1]);%rysowanie punkt�w na wykresie
s=[gx1(startPoint(1),startPoint(2)),gx2(startPoint(1),startPoint(2))];
%Szukanie lambdy 
fun = @(l) (f(startPoint(1)+s(1)*l,startPoint(2)+s(2)*l));
lambda = fminsearch(fun,0);

xi=startPoint+s*lambda; %nasz x1
xi=double(xi)
scatter3(xi(1),xi(2),f(xi(1),xi(2)),70,'MarkerFaceColor',[1 0 0]);%rysowanie punkt�w na wykresie
xTable=[xi];
if abs(f(xi(1),xi(2))-f(startPoint(1),startPoint(2)))<minStep
    displayResults(xi,matx,f,handles);
    return
end
%%%%
%%Wedle algorytmu powinni�my teraz zinkrementowa� i, no ale nie mielibysmy
%%wtedy x2 do wyliczania s2. Wi�c liczymy tutaj s2 w oparciu o x0 i x1- kt�ry zmodyfikuje x2
licznik=gx1(xi(1),xi(2))^2+gx2(xi(1),xi(2))^2;
mianownik=gx1(startPoint(1),startPoint(2))^2+gx2(startPoint(1),startPoint(2))^2;

si=[gx1(xi(1),xi(2)),gx2(xi(1),xi(2))]+licznik/mianownik*s(1,:);
s=[s;si];
i=2
%%I teraz liczymy x2 - zaczynaj�c od lambdy
fun=@(l) (f(xTable(1,1)+l*si(1),xTable(1,2)+l*si(2)));
    options = optimset('MaxFunEvals',100);
    lambda = fminsearch(fun,0,options);
xi=xi+si*lambda; %nasz x2
xi=double(xi)
scatter3(xi(1),xi(2),f(xi(1),xi(2)),'MarkerFaceColor',[1 0 0]);%rysowanie punkt�w na wykresie
while 1
    licznik=gx1(xi(1),xi(2))^2+gx2(xi(1),xi(2))^2;
    mianownik=gx1(xTable(i-1,1),xTable(i-1,2))^2+gx2(xTable(i-1,1),xTable(i-1,2))^2;
    si=[gx1(xi(1),xi(2)),gx2(xi(1),xi(2))]+licznik/mianownik*s(i,:);
    s=[s;si];
    %Szukamy d�ugo�ci lambda
    fun = @(l) (f(xTable(i-1,1)+l*si(1),xTable(i-1,2)+l*si(2)));
    options = optimset('MaxFunEvals',100);
    lambda = fminsearch(fun,0);
    xi=xTable(i-1,:)+lambda*si;%liczymy aktualne xi po zinkrementowaniu
    scatter3(xi(1),xi(2),f(xi(1),xi(2)),70,'MarkerFaceColor',[1 0 0]);%rysowanie punkt�w na wykresie
    xi=double(xi)
    if isnan(xi)%%kiedy juz zbiegli�my
        xi=xTable(i-1,:)
        break;
    end
    xTable=[xTable;xi];
    if abs(f(xi(1),xi(2))-f(xTable(i-1,1),xTable(i-1,2)))<minStep
        break;
    end
    i=i+1
    xi
end
displayResults(xi,matx,f,handles);


function clickThePoint(src,event)



function editMinX_Callback(hObject, eventdata, handles)
% hObject    handle to editMinX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editMinX as text
%        str2double(get(hObject,'String')) returns contents of editMinX as a double


% --- Executes during object creation, after setting all properties.
function editMinX_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editMinX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editMaxX_Callback(hObject, eventdata, handles)
% hObject    handle to editMaxX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editMaxX as text
%        str2double(get(hObject,'String')) returns contents of editMaxX as a double


% --- Executes during object creation, after setting all properties.
function editMaxX_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editMaxX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editMinY_Callback(hObject, eventdata, handles)
% hObject    handle to editMinY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editMinY as text
%        str2double(get(hObject,'String')) returns contents of editMinY as a double


% --- Executes during object creation, after setting all properties.
function editMinY_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editMinY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editMaxY_Callback(hObject, eventdata, handles)
% hObject    handle to editMaxY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editMaxY as text
%        str2double(get(hObject,'String')) returns contents of editMaxY as a double


% --- Executes during object creation, after setting all properties.
function editMaxY_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editMaxY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editNet_Callback(hObject, eventdata, handles)
% hObject    handle to editNet (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editNet as text
%        str2double(get(hObject,'String')) returns contents of editNet as a double


% --- Executes during object creation, after setting all properties.
function editNet_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editNet (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function rbtnUser_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rbtnUser (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in rbtnUser.
function rbtnUser_Callback(hObject, eventdata, handles)
handles.txtFn.Visible='on';
handles.editFn.Visible='on';
handles.pop.Visible='off';
handles.txtFunction.Visible='off';
% hObject    handle to rbtnUser (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbtnUser


% --- Executes on button press in rbtnList.
function rbtnList_Callback(hObject, eventdata, handles)
handles.txtFn.Visible='off';
handles.editFn.Visible='off';
handles.pop.Visible='on';
handles.txtFunction.Visible='on';
functionstr=chooseFunction(handles.pop.Value);
functionstr=strrep(functionstr,'@(x,y)','f(x,y)=');
handles.txtFunction.String=functionstr;
% hObject    handle to rbtnList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbtnList


% --- Executes on button press in rbtnIteraton.
function rbtnIteraton_Callback(hObject, eventdata, handles)
handles.txtStep.Visible='off';
handles.editStep.Visible='off';
handles.txtIterationCount.Visible='on';
handles.sliderIteration.Visible='on';

% hObject    handle to rbtnIteraton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbtnIteraton


% --- Executes on button press in rbtnStep.
function rbtnStep_Callback(hObject, eventdata, handles)
handles.txtStep.Visible='on';
handles.editStep.Visible='on';
handles.txtIterationCount.Visible='off';
handles.sliderIteration.Visible='off';
% hObject    handle to rbtnStep (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbtnStep



function editLambda_Callback(hObject, eventdata, handles)
% hObject    handle to editLambda (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editLambda as text
%        str2double(get(hObject,'String')) returns contents of editLambda as a double


% --- Executes during object creation, after setting all properties.
function editLambda_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editLambda (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
